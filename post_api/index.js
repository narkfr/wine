// index.js
/*global require, console*/
"use strict";
const request = require('request');
const serialport = require('serialport');
const configData = require('./config');
const config = configData.config;

// Set the sensor to the serialPort
const SerialPort = serialport.SerialPort;
const sensor = new SerialPort(config.port, {
    baudrate: 9600,
    parser: serialport.parsers.readline("\n")
});

// Define the url for the post, from config.js file
let url =
        config.protocole +
        config.username + ':' +
        config.password + '@' +
        config.path;

// Post the data
const sendData = function (data) {
    request.post({
        url: url,
        form: {
            "temperature": data.temperature,
            "humidity": data.humidity
        }
    }, function (response) {
        // console is not recommended in production, uncomment this for your dev
        //console.log(response);
    });
};

// Get the data and prepare the for the post
sensor.open(function () {
    sensor.on('data', function (datain) {
        let datajson = JSON.parse(datain),
            temperature = parseInt(datajson.temp, 10),
            humidity = parseInt(datajson.hum, 10),
            data = {
                "temperature": temperature,
                "humidity": humidity
            };
        sendData(data);
    });
});
