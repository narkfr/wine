'use strict';
/*eslint no-console: [2, { allow: ["table", "log", "warn", "error"] }] */
import React from 'react';
import $ from 'jquery';
import LineChart from 'react-d3-components/lib/LineChart';
import Grid from 'react-bootstrap/lib/Grid';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';
import Jumbotron from 'react-bootstrap/lib/Jumbotron';

require('styles//Charts.css');

export default React.createClass({
  getInitialState() {
    return {
      current: '',
      dataTemperature: [{
        label: 'Temperature',
        values: [{x:'', y:0}]
      }],
      dataHumidity: [{
        label: 'Humidity',
        values: [{x:'', y:0}]
      }]
    };
  },

  // return date in a more human readable format
  parseDate(data) {
    let date = new Date(data);
    let month = date.getMonth()+1;
    let minutes = date.getMinutes();
    if (month < 10) {
      month = '0'+ month;
    } else {
      month = month;
    }
    if (minutes < 10) {
      minutes = '0'+ date.getMinutes();
    } else {
      minutes = date.getMinutes();
    }
    return {
      day: date.getDate(),
      month: month,
      hours: date.getHours(),
      minutes: minutes
    }
  },

  // get the data, and return an array ok for D3
  parseValues(data) {
    const self = this;
    let temperatureArray = [];
    let humidityArray = [];
    for (let i in data) {
      let date = self.parseDate(data[i].data_date);
      let temperatureItem = {
        x: date.hours + 'h' + date.minutes,
        y: data[i].temperature
      };
      let humidityItem = {
        x: date.hours + 'h' + date.minutes,
        y: data[i].humidity
      };
      temperatureArray.push(temperatureItem);
      humidityArray.push(humidityItem);
    }
    return {
      temperature: temperatureArray.reverse(),
      humidity: humidityArray.reverse()
    }
  },

  componentDidMount() {
    const self = this;
    // Get the data, and update state
    self.serverRequest = $.ajax({
      url: self.props.source,
      type: 'GET',
      success: function (result) {
        let date = self.parseDate(result.results[0].data_date);
        let values = self.parseValues(result.results);
        self.setState({
          current: {
            temperature: result.results[0].temperature,
            humidity: result.results[0].humidity,
            date: date.day + '/' + date.month + ' ' + date.hours + 'h' + date.minutes
          },
          dataTemperature: {
            values: values.temperature
          },
          dataHumidity: {
            values: values.humidity
          }
        });
      }
    });
  },

  render() {
    const self = this;
    return (
      <Grid className="charts-component container">
        <Row className='text-center'>
          <h1>Capteur de température et d humidité</h1>
          <h3>Basé sur un catpeur Arduino, relié à un Raspberry Pi qui envoie les infos à un back-end Django via un script NodeJS.</h3>
          <h3>Le front utilise ReactJS</h3>
        </Row>
        <Row>
          <Col md={6} className="text-center">
            <Jumbotron>
              <h4>{self.state.current.date}</h4>
              <h2>Température</h2>
              <h1>{self.state.current.temperature}</h1>
              <h2>Humidité</h2>
              <h1>
                {self.state.current.humidity} %<br/>
              </h1>
            </Jumbotron>
          </Col>
          <Col md={6} className="text-center">
            <LineChart
              data={self.state.dataTemperature}
              width={800}
              height={400}
              margin={{top: 10, bottom: 50, left: 50, right: 10}}
            />
            <h4>Température des dernières 4h</h4>
            <LineChart
              data={self.state.dataHumidity}
              width={800}
              height={400}
              margin={{top: 10, bottom: 50, left: 50, right: 10}}
            />
            <h4>Humidité des dernières 4h</h4>
          </Col>
        </Row>
      </Grid>
    );
  }
});
