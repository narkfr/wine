# Wine cave


This project is a full stack application to manage wine cave:

  - it display charts of temperature & humidity
  - it allow you to manage your bottles collection *WIP*

It use Arduino, Raspberry Pi, ReactJS & Django.

This documentation isn't exhaustive, you need some basic knowledges in Arduino, Debian, Javascript & Django

#### Installation

`git clone git@github.com:Narkfr/nark_wine_cellar.git`

Then follow instructions below


## Arduino

Hardware part use a DHT22 sensor to get current temperature & humidity. It make a mesure every 15 minutes, and send it to the serialport.

DHT22 is very easy to use, you can find more information about it [here](http://www.instructables.com/id/How-to-use-DHT-22-sensor-Arduino-Tutorial/).

#### Installation

Install the `DHT.h` lib from DHT.zip in your local arduino lib path.

Then upload `sensor_ino.ino` to your Arduino board.

## Post_API

The Arduino is plugged on a Raspberry Pi, on Raspbian. It run a NodeJS arm compatible version. The rPi & his installation are not detailled here.

#### Installation

`cd post_api && npm install`

You need to define some settings in a `config.js` file. You can copy the `config_example.js` file then fill it.

`cp config_example.js config.js`

#### Run the app

Then you can run the app.

`npm start`

You can use something like [forever](https://www.npmjs.com/package/forever) to run your app permanently on your Pi.

I make my prototype with an 'old' Raspberry Pi 1 A, and, meh, it's a bit slow, but itjustworks©.

## Wine_cellar

This part is a minimal Rest API, using [Django](https://www.djangoproject.com/) & [Django Rest Framework](http://www.django-rest-framework.org/)

It's still in development, I don't recommend to use it in production.

#### Installation

It's recommended to run your Django App in a venv :

`virtualenv --no-site-packages venv && source venv/bin/activate`

Then you can install your application :

`cd wine_cellar && pip install -r requirements.txt`

Run the firsts migrations:

`python manage.py migrate`

Then create the super user:

`python manage.py createsuperuser`

#### Run the app

`python manage.py runserver 127.0.0.1:8080`

You can use your super user in the config.js file of the post_api (but is not recommended), or create a specific user for this particular post (it's a much better idea).

## Front_React

This front app display the last temperature & humidity mesured, and 2 charts of the last 4 hours (you can set this in your Django API).

It's a reactJS app, generated with [yeoman react webpack generator](https://github.com/newtriks/generator-react-webpack). If you need more informations for build / test / env, rtfm.

#### Installation

`cd front_react && npm install`

#### Run the app

`npm start`

#### Settings

You have to set the 'source' to get the data in the Main.js component:

`<Charts source='path/to/your/api'/>`

## TODO

  - move api source in a config file for front
  - secure the django api with cors and specific user group
  - i18n for front
  - create front for bottle collection
