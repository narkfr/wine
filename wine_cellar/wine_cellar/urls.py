from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
import users.views
import temps.views
import collection.views

router = routers.DefaultRouter()
#router.register(r'users', users.views.UserViewSet)
#router.register(r'groups', users.views.GroupViewSet)
router.register(r'temps', temps.views.DataWeatherViewSet)
#router.register(r'collection', collection.views.CollectionViewSet)

urlpatterns = [
    #url('^admin/', include(admin.site.urls)),
    url(r'^', include(router.urls)),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
