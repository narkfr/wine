from django.db import models
from datetime import datetime

class DataWeather(models.Model):
    temperature = models.IntegerField(default=0)
    humidity = models.IntegerField(default=0)
    data_date = models.DateTimeField(default=datetime.now, blank=True)
