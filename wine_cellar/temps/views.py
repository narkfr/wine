from django.http import HttpResponse
from temps.models import DataWeather
from rest_framework import viewsets
from temps.serializers import DataWeatherSerializer

def index(request):
    return HttpResponse("Hello, world. You're at the temps index.")

class DataWeatherViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows data to be viewed or edited.
    """
    queryset = DataWeather.objects.all().order_by('-data_date')
    serializer_class = DataWeatherSerializer
