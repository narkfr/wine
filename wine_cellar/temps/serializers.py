from temps.models import DataWeather
from rest_framework import serializers


class DataWeatherSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DataWeather
        fields = ('temperature', 'humidity', 'data_date')
