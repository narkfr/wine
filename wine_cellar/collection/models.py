from django.db import models

class WineBottle(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    wine_type = models.CharField(max_length=200)
    zip_code = models.IntegerField(default=0)
    year = models.IntegerField(default=0)
