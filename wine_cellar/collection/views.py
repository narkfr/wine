from collection.models import WineBottle
from rest_framework import viewsets
from collection.serializers import CollectionSerializer


class CollectionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows wine to be viewed or edited.
    """
    queryset = WineBottle.objects.all().order_by('-year')
    serializer_class = CollectionSerializer
