from collection.models import WineBottle
from rest_framework import serializers


class CollectionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WineBottle
        fields = ('name', 'year', 'wine_type', 'zip_code')
