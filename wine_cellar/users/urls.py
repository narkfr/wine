from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'users', views.UserViewSet),
    url(r'groups', views.GroupViewSet),
]
